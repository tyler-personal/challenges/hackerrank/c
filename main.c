#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

struct obstacle {
    int row;
    int col;
};

int main(void) {
    int size, obstacleCount, row, col;
    scanf("%d %d", &size, &obstacleCount);
    scanf("%d %d", &row, &col);

    struct obstacle obstacles[obstacleCount];
    for (int i = 0; i < obstacleCount; i++) {
        int obstacleRow, obstacleCol;
        scanf("%d %d", &obstacleRow, &obstacleCol);
        obstacles[i] = (struct obstacle){obstacleRow, obstacleCol};
    }

    printf("%d\n", size+obstacleCount);

    return EXIT_SUCCESS;
}
